$(function () {
  $('a[href*="#"]')
    // Remove links that don't actually link to anything
    .not('[href="#"]')
    .not('[href="#0"]')
    .click(function (event) {
      // On-page links
      if (
        location.pathname.replace(/^\//, '') ==
          this.pathname.replace(/^\//, '') &&
        location.hostname == this.hostname
      ) {
        // Figure out element to scroll to
        var target = $(this.hash);
        target = target.length
          ? target
          : $('[name=' + this.hash.slice(1) + ']');
        // Does a scroll target exist?
        if (target.length) {
          // Only prevent default if animation is actually gonna happen
          event.preventDefault();
          $('html, body').animate(
            {
              scrollTop: target.offset().top,
            },
            1000,
            function () {
              // Callback after animation
              // Must change focus!
              var $target = $(target);
              $target.focus();
              if ($target.is(':focus')) {
                // Checking if the target was focused
                return false;
              } else {
                $target.attr('tabindex', '-1'); // Adding tabindex for elements not focusable
                $target.focus(); // Set focus again
              }
            }
          );
        }
      }
    });

  $('.phone').inputmask({
    mask: '+7(999)-999-99-99',
    showMaskOnHover: false,
  });

  $('.open-modal').on('click', function (e) {
    e.preventDefault();

    $('.l-modal').toggle();
  });

  $('.l-modal__centered').on('click', function (e) {
    e.preventDefault();

    if (e.target.className === 'l-modal__centered') {
      $('.l-modal').hide();
    }
  });

  $('.l-modal__close').on('click', function (e) {
    e.preventDefault();
    $('.l-modal').hide();
  });

  $('.open-thx').on('click', function (e) {
    e.preventDefault();

    $('.l-thx').toggle();
  });

  $('.l-thx__centered').on('click', function (e) {
    e.preventDefault();

    if (e.target.className === 'l-thx__centered') {
      $('.l-thx').hide();
    }
  });

  $('.l-thx__close').on('click', function (e) {
    e.preventDefault();
    $('.l-thx').hide();
  });

  $('.open-contacts').on('click', function (e) {
    e.preventDefault();

    $('.l-contacts').toggle();
  });

  $('.l-contacts__centered').on('click', function (e) {
    e.preventDefault();

    if (e.target.className === 'l-contacts__centered') {
      $('.l-contacts').hide();
    }
  });

  $('.l-contacts__close').on('click', function (e) {
    e.preventDefault();
    $('.l-contacts').hide();
  });

  $('.l-tasks-card__centered').on('click', function (e) {
    e.preventDefault();

    if (!$(this).parent().hasClass('l-tasks-card_active')) {
      if (e.target.className === 'l-tasks-card__centered') {
        $('.l-tasks-card_active').each(function () {
          $(this).toggleClass('l-tasks-card_active');
        });

        $('.l-tasks-card').each(function () {
          $(this).toggleClass('l-tasks-card_hide');
        });
        $(this).parent().toggleClass('l-tasks-card_hide');
        $(this).parent().toggleClass('l-tasks-card_active');
        $([document.documentElement, document.body]).animate(
          {
            scrollTop: $(this).offset().top,
          },
          500
        );
      }
    }
  });

  $('.l-tasks-card__close').on('click', function (e) {
    e.stopPropagation();

    $('.l-tasks-card').each(function () {
      $(this).removeClass('l-tasks-card_hide l-tasks-card_active');
    });
  });

  /* $(".l-tasks-card__close").on("click", function (e) {
    e.preventDefault();

    $(".l-tasks-card").each(function () {
      $(this).toggleClass("l-tasks-card_hide");
      $(this).toggleClass("l-tasks-card_active");
    });
  }); */

  new Swiper('.l-warranty__cards', {
    navigation: {
      nextEl: '.l-warranty .swiper-button-next',
      prevEl: '.l-warranty .swiper-button-prev',
    },
    loop: true,
    spaceBetween: 70,
    slidesPerView: 1,
    breakpoints: {
      1024: {
        slidesPerView: 2,
      },
    },
  });

  $(window).on('load resize', function () {
    var cw = $('.l-gallery-card').width();
    $('.l-gallery-card').css({ height: cw + 'px' });
  });
});
